from rest_framework import serializers
from .models import Brick, Item, MasterData, PreferredItem
class BrickSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Brick
        fields = '__all__'

class ItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'

class MasterDataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MasterData
        fields = '__all__'

class PreferredItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'
