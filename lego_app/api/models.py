from django.db import models
import random

# Create your models here.
class Brick(models.Model):

    designid = models.IntegerField(primary_key=True)
    colorCode = models.CharField(max_length=150)

class Item(models.Model):

    bricks = models.ManyToManyField(Brick)

class MasterData(models.Model):

    STATUS_CHOICES = (
        (1, "Normal"),
        (2, "Novelty"),
        (3, "Outgoing"),
        (4, "Discontinued")
    )

    item = models.ForeignKey(Item,on_delete=models.CASCADE)
    price = models.FloatField(default=round(random.uniform(9.99, 99.00), 2))
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=random.uniform(1, 4))

class PreferredItem(models.Model):

    pref_item = models.ManyToManyField(Item)
