from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Brick, Item, MasterData, PreferredItem
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import BrickSerializer, ItemSerializer, MasterDataSerializer, PreferredItemSerializer

from rest_framework.views import APIView
from rest_framework.response import Response

from django.forms.models import model_to_dict
# Create your views here.

def root(request):
    return HttpResponse("Hello!!")

class PreferredItemService(APIView):

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        filter = request.data.get("bricks")
        selected = Item.objects.filter(bricks__in=filter)
        Items = ItemSerializer(selected, many=True,context={'request': request})
        return Response({'status': 200, "preferred_items": Items.data})

class MasterDataViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = MasterData.objects.all()
    serializer_class = MasterDataSerializer
    #permission_classes = [permissions.IsAuthenticated]

class PreferredItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = PreferredItem.objects.all()
    serializer_class = PreferredItemSerializer
    #permission_classes = [permissions.IsAuthenticated]

class BrickViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Brick.objects.all()
    serializer_class = BrickSerializer
    #permission_classes = [permissions.IsAuthenticated]


class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    #permission_classes = [permissions.IsAuthenticated]

