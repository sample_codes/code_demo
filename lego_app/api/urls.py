
from django.urls import path,include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'bricks', views.BrickViewSet)
router.register(r'items', views.ItemViewSet)
router.register(r'masterdata', views.MasterDataViewSet)
#router.register(r'preferreditem', views.PreferredItemViewSet)

urlpatterns = [
    #path('', views.root),
    path('preferredItem',views.PreferredItemService.as_view()),
    path('', include(router.urls)),
]