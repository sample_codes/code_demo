import requests
import random

BASE_URL_brick = "http://localhost:8080/bricks/"
BASE_URL_item = "http://localhost:8080/items/"

def test_ping():
    response = requests.get(BASE_URL_brick)
    assert response.status_code == 200
def test_createbricks():

    data = {
        "colorCode": ""
        }
    counter = int(random.uniform(7, 10))
    passcounter = 0
    for x in range(0,counter):
        data['colorCode'] = ','.join([str(int(random.uniform(10,99))) for x in range(0,int(random.uniform(2,6)))])
        response = requests.post(BASE_URL_brick,data=data)
        assert response.status_code == 201

    print(f"\n -- created {counter} new bricks")

def get_bricks():
    output = list()
    url = BASE_URL_brick
    while True:
        response = requests.get(url)
        data = response.json()
        output.extend(data['results'])
        if data['next']:
            url = data['next']
        else:
            break

    return output

def test_createitems():

    input = get_bricks()
    data = {"bricks": []}
    print(f"\n -- found {len(input)} bricks")
    for i in range(0,3):
        sample = random.sample(input,int(random.uniform(1,5)))
        data['bricks'] = [x['url'] for x in sample]
        response = requests.post(BASE_URL_item, data=data)
        assert response.status_code == 201
    print(f"-- created {len(sample)} items")