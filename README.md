# Lego App 
| The App is coded in Django + REST Framework + SQlite Stack

Have choosen a Monolith like approach instead of individual Microservices approach (not that familiar with flask/fastapi )
<h3 style="text-align: center;">Arch Diagram</h3>

![API Views](docs/ProcessFlow.png "Arch Diagram")

### Test Scenarios

- [x] sanity/ping tests
- [x] creating bricks/Item through apis
- [x] creating masterdata/prefered items through apis
- [ ] prefered item constraints

![API Views](docs/screens.gif "APis")

### Cloud Deployments Process
```commandline
    docker build -t lego-service .
    docker run -dp 80:8000 lego-service
    docker push
```
