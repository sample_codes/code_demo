FROM python:3.10-slim-bullseye
# setting work directory
WORKDIR /usr/src/app
COPY ./lego_app/ /usr/src/app

# env variables
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWEITEBYTECODE 1

RUN pip install --upgrade pip pipenv

RUN pipenv install --system --ignore-pipfile

EXPOSE 8000
ENTRYPOINT ["python","manage.py","runserver","0.0.0.0:8000"]
